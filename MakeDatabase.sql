DROP TABLE IF EXISTS survey_response;
DROP TABLE IF EXISTS choice;
DROP TABLE IF EXISTS question;


CREATE TABLE question(
	id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	content VARCHAR(255),
	allowMultipleAnswers BOOL DEFAULT FALSE,
	sortorder INT
);

CREATE TABLE choice(
	id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	content VARCHAR(255),
	sortorder int,
	question INT NOT NULL,
	FOREIGN KEY(question) REFERENCES question(id)
);

CREATE TABLE survey_response(
	id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	user_id INT,
	choice INT,
	FOREIGN KEY(choice) REFERENCES choice(id)
);

INSERT INTO question(content,allowMultipleAnswers,sortorder) VALUES('How old are you?',false,1);
INSERT INTO question(content,allowMultipleAnswers,sortorder) VALUES('Are you happy?',false,2);
INSERT INTO question(content,allowMultipleAnswers,sortorder) VALUES('What countries have you visited?',true,3);
INSERT INTO question(content,allowMultipleAnswers,sortorder) VALUES('What is your favorite sport?',false,4);
INSERT INTO question(content,allowMultipleAnswers,sortorder) VALUES('What programming languages do you know?',true,5);

INSERT INTO choice(content,sortorder,question) VALUES('Less than 18',1,1);
INSERT INTO choice(content,sortorder,question) VALUES('18-99',2,1);
INSERT INTO choice(content,sortorder,question) VALUES('More than 99',3,1);

INSERT INTO choice(content,sortorder,question) VALUES('Yes',1,2);
INSERT INTO choice(content,sortorder,question) VALUES('No',2,2);

INSERT INTO choice(content,sortorder,question) VALUES('Spain',1,3);
INSERT INTO choice(content,sortorder,question) VALUES('France',2,3);
INSERT INTO choice(content,sortorder,question) VALUES('Italy',3,3);
INSERT INTO choice(content,sortorder,question) VALUES('England',4,3);
INSERT INTO choice(content,sortorder,question) VALUES('Portugal',5,3);

INSERT INTO choice(content,sortorder,question) VALUES('Football',1,4);
INSERT INTO choice(content,sortorder,question) VALUES('Basketball',2,4);
INSERT INTO choice(content,sortorder,question) VALUES('Soccer',3,4);
INSERT INTO choice(content,sortorder,question) VALUES('Volleyball',4,4);

INSERT INTO choice(content,sortorder,question) VALUES('PHP',1,5);
INSERT INTO choice(content,sortorder,question) VALUES('Ruby',2,5);
INSERT INTO choice(content,sortorder,question) VALUES('Javascript',3,5);
INSERT INTO choice(content,sortorder,question) VALUES('Python',4,5);
