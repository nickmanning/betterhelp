var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var resultsRouter = require('./routes/results');


const mysql = require('mysql');
const connection = mysql.createConnection({
  user: "root",
  password: "plplpl",
  host: "127.0.0.1",
  database: "betterhelp",
  multipleStatements: true
});
connection.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});

var app = express();

app.use(function (req, res, next) {
  req.connection = connection;
  next()
})


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'twig');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.post('/submit',function(req,res){
  req.connection.query("SELECT MAX(user_id) as max FROM survey_response",function(err,rows){
    const userId = rows[0].max ? rows[0].max + 1 : 1;
    const queryList = Object.keys(req.body).reduce((list,questionId)=>{
      const answer = req.body[questionId];
      if (Array.isArray(answer)){
        answer.forEach(a=>{
          list.push(`INSERT INTO survey_response(user_id,choice) VALUES(${userId},${a})`);
        })
      }
      else{
        list.push(`INSERT INTO survey_response(user_id,choice) VALUES(${userId},${answer})`);
      }
      
      return list;
    },[]);
    req.connection.query(queryList.join(";"),function(err,rows){
      if (err) console.log(err);
      res.redirect('/');
    })
    
  
    
  })
  
})

app.use('/', indexRouter);
app.use('/results', resultsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});



module.exports = app;
