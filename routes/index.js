var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  req.connection.query(`SELECT question.content as 'question', 
  choice.content as 'answer', 
  question.allowMultipleAnswers,
  question.sortorder,
  question.id as questionId,
  choice.id as answerId 
  FROM choice INNER JOIN question 
  ON choice.question = question.id
  ORDER BY question.sortorder, choice.sortorder;
  `,function(err,response){
   
    res.render('index', { questions: response.reduce((arr,row)=>{
      if (arr.length===0 || arr[arr.length-1].content !== row.question){
        arr.push({
          content:row.question,
          allowMultipleAnswers:row.allowMultipleAnswers,
          id:row.questionId,
          answers:[
            {
              content:row.answer,
              id:row.answerId
            }
          ]
        });
      }
      else{
        arr[arr.length - 1].answers.push({
          content:row.answer,
          id:row.answerId
        })
      }
      return arr;
    },[]) });
  })
});

module.exports = router;
