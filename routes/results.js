var express = require('express');
var router = express.Router();
var groupBy = require('lodash.groupby');

const iAmHappyId = 4;
const iAmSadId = 5;

function getChoiceCountTableQuery(isHappy){
  return ` SELECT question.content as question,  question.id as question_id, choice.content as choice, COUNT(*) as ${isHappy?'happy':'sad'}_count FROM survey_response
INNER JOIN (SELECT user_id as id FROM survey_response WHERE choice = ${isHappy?iAmHappyId:iAmSadId}) AS ${isHappy?'happy':'sad'}User ON survey_response.user_id = ${isHappy?'happy':'sad'}User.id
INNER JOIN choice on survey_response.choice = choice.id
INNER JOIN question on choice.question = question.id
GROUP BY choice
) as ${isHappy?'happy':'sad'}ChoiceCount`
}

function getMaxQuestionTableQuery(isHappy){
  return `SELECT question, MAX(${isHappy?'happy':'sad'}_count) as max_question FROM (
    SELECT question.content as question, choice.content as choice, COUNT(*) as ${isHappy?'happy':'sad'}_count FROM survey_response
    INNER JOIN (SELECT user_id as id FROM survey_response WHERE choice = ${isHappy?iAmHappyId:iAmSadId}) AS ${isHappy?'happy':'sad'}User ON survey_response.user_id = ${isHappy?'happy':'sad'}User.id
    INNER JOIN choice on survey_response.choice = choice.id
    INNER JOIN question on choice.question = question.id
    GROUP BY choice
  ) as maxPerQuestion`;
}


const happyQuery = `SELECT * FROM (
  ${getChoiceCountTableQuery(true)}

  INNER JOIN (
  ${getMaxQuestionTableQuery(true)}
  GROUP BY question
  ) as happyQuestionMax
  ON happyChoiceCount.question = happyQuestionMax.question AND happyChoiceCount.happy_count = happyQuestionMax.max_question
  WHERE happyChoiceCount.question_id!=2;

  `;

  

    
  

  const sadQuery = `SELECT * FROM (
    ${getChoiceCountTableQuery(false)}
    INNER JOIN (
    ${getMaxQuestionTableQuery(false)}

    GROUP BY question
    ) as sadQuestionMax
    ON sadChoiceCount.question = sadQuestionMax.question AND sadChoiceCount.sad_count = sadQuestionMax.max_question
    WHERE sadChoiceCount.question_id!=2;

  `;

/* GET users listing. */
router.get('/', function(req, res, next) {

  console.log(happyQuery);



  req.connection.query(happyQuery+sadQuery,(err,results)=>{
    const happyAnswersGrouped = groupBy(results[0],'question');
    const happyAnswers = Object.keys(happyAnswersGrouped).map(key=>{
      return {
        question:key,
        answers:happyAnswersGrouped[key].map(ans=>ans.choice).join(", ")
      }
    });

    const sadAnswersGrouped = groupBy(results[1],'question');
    const sadAnswers = Object.keys(sadAnswersGrouped).map(key=>{
      return {
        question:key,
        answers:sadAnswersGrouped[key].map(ans=>ans.choice).join(", ")
      }
    });
    console.log(sadAnswers)
    res.render('results',{
      happyAnswers, sadAnswers
    });
  })
});

module.exports = router;
